# Trabalho 1

## Objetivo 

O objetivo desse trabalho foi habituar os alunos a utilizarem a ferramenta git,
bem como entender os principais usos de C++ e seu compilador. Também foi
apresentado o debugger (gdb) para depurar erros na execução do código.

## Requisitos

 - Sistema Operacional Linux
 - Ferramenta make (sudo apt-get install make)
 - Instalar biblioteca gráfica OpenGL
 - Compilador C++ (gcc version 5.4.0 20160609 (Ubuntu 5.4.0-6ubuntu1~16.04.4))


 
## Documentação em Doxygen
![Doxygen documentation](html/index.html)


# Parte 1 - Bouncing Ball 

## Como compilar

 - ```make```
 - ```./test-ball```
 

## Descrição Bouncing Ball

O repositório contém um arquivo ball.cpp, juntamente com os arquivos para teste
da função test-ball.cpp.
Ainda em ball.cpp, estão situados o objeto Ball e seus respectivos métodos 
step() e display() como chamadas das funções implementadas no header ball.h, bem
como a declaração de suas variáveis.

Existe também os arquivos Graphics.cpp, cujo os métodos traduzem a interface 
gráfica do programa desenvolvido em ball.cpp, bem como um header Graphics.h, que
declara os métodos e variáveis que estarão presentes em Graphics.cpp.

Por fim, no repositório também se localiza a Simulation.h, que é um header que
contém as declarações dos métodos virtuais implementados.

## Coordenadas X Y 

0.01 -0.00877778 <br/>
0.02 -0.0284444 <br/>
0.03 -0.059<br/>
0.04 -0.100444<br/>
0.05 -0.152778<br/>
0.06 -0.216<br/>
0.07 -0.290111<br/>
0.08 -0.375111<br/>
0.09 -0.471<br/>
0.1 -0.577778<br/>
0.11 -0.695444<br/>
0.12 -0.824<br/>
0.13 -0.824<br/>
0.14 -0.695444<br/>
0.15 -0.577778<br/>
0.16 -0.471<br/>
0.17 -0.375111<br/>
0.18 -0.290111<br/>
0.19 -0.216<br/>
0.2 -0.152778<br/>
0.21 -0.100444<br/>
0.22 -0.059<br/>
0.23 -0.0284444<br/>
0.24 -0.00877778<br/>
0.25 -8.1532e-17<br/>
0.26 -0.00211111<br/>
0.27 -0.0151111<br/>
0.28 -0.039<br/>
0.29 -0.0737778<br/>
0.3 -0.119444<br/>
0.31 -0.176<br/>
0.32 -0.243444<br/>
0.33 -0.321778<br/>
0.34 -0.411<br/>
0.35 -0.511111<br/>
0.36 -0.622111<br/>
0.37 -0.744<br/>
0.38 -0.876778<br/>
0.39 -0.876778<br/>
0.4 -0.744<br/>
0.41 -0.622111<br/>
0.42 -0.511111<br/>
0.43 -0.411<br/>
0.44 -0.321778<br/>
0.45 -0.243444<br/>
0.46 -0.176<br/>
0.47 -0.119444<br/>
0.48 -0.0737778<br/>
0.49 -0.039<br/>
0.5 -0.0151111<br/>
0.51 -0.00211111<br/>
0.52 -4.90059e-16<br/>
0.53 -0.00877778<br/>
0.54 -0.0284444<br/>
0.55 -0.059<br/>
0.56 -0.100444<br/>
0.57 -0.152778<br/>
0.58 -0.216<br/>
0.59 -0.290111<br/>
0.6 -0.375111<br/>
0.61 -0.471<br/>
0.62 -0.577778<br/>
0.63 -0.695444<br/>
0.64 -0.824<br/>
0.65 -0.824<br/>
0.66 -0.695444<br/>
0.67 -0.577778<br/>
0.68 -0.471<br/>
0.69 -0.375111<br/>
0.7 -0.290111<br/>
0.71 -0.216<br/>
0.72 -0.152778<br/>
0.73 -0.100444<br/>
0.74 -0.059<br/>
0.75 -0.0284444<br/>
0.76 -0.00877778<br/>
0.77 -7.47666e-16<br/>
0.78 -0.00211111<br/>
0.79 -0.0151111<br/>
0.8 -0.039<br/>
0.81 -0.0737778<br/>
0.82 -0.119444<br/>
0.83 -0.176<br/>
0.84 -0.243444<br/>
0.85 -0.321778<br/>
0.86 -0.411<br/>
0.87 -0.511111<br/>
0.88 -0.622111<br/>
0.89 -0.744<br/>
0.89 -0.876778<br/>
0.88 -0.876778<br/>
0.87 -0.744<br/>
0.86 -0.622111<br/>
0.85 -0.511111<br/>
0.84 -0.411<br/>
0.83 -0.321778<br/>
0.82 -0.243444<br/>
0.81 -0.176<br/>
0.8 -0.119444<br/>
0.79 -0.0737778<br/>


## Diagrama de classes
Para essa fase do trabalho foi utilizado o Doxygen e para verificar o diagrama
de classes, basta entrar na pasta e abrir o arquivo index.html em seu navegador.

![Alt text](html/classBallDrawable__inherit__graph.png?raw=true "")
 
 
## Gráfico 
- Bouncing Ball ![Alt text](grafico.png?raw=true "Optional Title")

# Parte 2 - SpringMass

## Como compilar

 - ```make```
 - ```./test-springmass```

## Descrição SpringMass

O repositório contém um arquivo springmass.cpp, além disso possui a springmass.h,
que é seu header e o test-springmass.cpp, que possui as funções para visualizar o
springmass.cpp. Se trata de um sistema com um oscilador harmônico amortecido, onde
são feitos cálculos de força, velocidade, energia total, entre outros. Existe uma
melhor documentação dos métodos mostrados no Diagrama de Classes

## Coordenadas X e Y

0.499444 -0.0027 <br/>
-0.499444 -0.0027<br/>
0.49779 -0.00717778<br/>
-0.49779 -0.00722222<br/>
0.495086 -0.0451721<br/>
-0.495086 0.0181721<br/>
0.491429 -0.0448044<br/>
-0.491429 0.00160441<br/>
0.486961 -0.0259005<br/>
-0.486961 -0.0370995<br/>
0.481862 -0.050249<br/>
-0.481862 -0.036151<br/>
0.476345 -0.0346323<br/>
-0.476345 -0.0787677<br/>
0.470645 -0.0615415<br/>
-0.470645 -0.0824585<br/>
0.465012 -0.111238<br/>
-0.465012 -0.0669618<br/>
0.459698 -0.122093<br/>
-0.459698 -0.0939069<br/>
0.454946 -0.114014<br/>
-0.454946 -0.143386<br/>
0.450979 -0.148774<br/>
-0.450979 -0.153626<br/>
0.447992 -0.206656<br/>
-0.447992 -0.144344<br/>
0.446139 -0.226205<br/>
-0.446139 -0.176995<br/>
0.445528 -0.227376<br/>
-0.445528 -0.231624<br/>
0.446212 -0.271885<br/>
-0.446212 -0.246515<br/>
0.448192 -0.276895<br/>
-0.448192 -0.304505<br/>
0.451407 -0.324654<br/>
-0.451407 -0.323346<br/>
0.455742 -0.332437<br/>
-0.455742 -0.385763<br/>
0.461029 -0.382308<br/>
-0.461029 -0.409692<br/>
0.467055 -0.454697<br/>
-0.467055 -0.414703<br/>
0.473567 -0.488485<br/>
-0.473567 -0.461915<br/>
0.480288 -0.50349<br/>
-0.480288 -0.53151<br/>
0.486924 -0.561097<br/>
-0.486924 -0.562103<br/>
0.493177 -0.641812<br/>
-0.493177 -0.573188<br/>
0.498761 -0.6846<br/>
-0.498761 -0.6258<br/>
0.503413 -0.709332<br/>
-0.503413 -0.700068<br/>
0.506905 -0.714452<br/>
-0.506905 -0.797548<br/>
0.509058 -0.760579<br/>
-0.509058 -0.857621<br/>
0.509744 -0.827284<br/>
-0.509744 -0.900716<br/>
0.508901 -0.915556<br/>
-0.508901 -0.925844<br/>
0.506533 -0.915556<br/>
-0.506533 -0.931154<br/>
0.502711 -0.84911<br/>
-0.502711 -0.917525<br/>
0.497573 -0.803493<br/>
-0.497573 -0.886667<br/>
0.491317 -0.778848<br/>
-0.491317 -0.838437<br/>
0.484196 -0.77616<br/>
-0.484196 -0.77185<br/>
0.476509 -0.733663<br/>
-0.476509 -0.748673<br/>
0.468584 -0.734847<br/>
-0.468584 -0.685414<br/>
0.460768 -0.697235<br/>
-0.460768 -0.664552<br/>
0.453411 -0.640744<br/>
-0.453411 -0.666167<br/>
0.446849 -0.627247<br/>
-0.446849 -0.62839<br/>
0.441394 -0.636983<br/>
-0.441394 -0.570979<br/>
0.43731 -0.608407<br/>
-0.43731 -0.555481<br/>
0.434811 -0.561509<br/>
-0.434811 -0.561904<br/>
0.434042 -0.558086<br/>
-0.434042 -0.528453<br/>
0.435077 -0.515227<br/>
-0.435077 -0.538037<br/>
0.437909 -0.515252<br/>
-0.437909 -0.508337<br/>
0.442453 -0.475429<br/>
-0.442453 -0.522085<br/>
0.448544 -0.477847<br/>
-0.448544 -0.497192<br/>
0.455946 -0.502957<br/>
-0.455946 -0.453208<br/>
0.46436 -0.489749<br/>
-0.46436 -0.451142<br/>
0.473433 -0.458081<br/>
-0.473433 -0.471134<br/>
0.482778 -0.469322<br/>
-0.482778 -0.451819<br/>
0.491984 -0.441121<br/>
-0.491984 -0.475545<br/>
0.500641 -0.455346<br/>
-0.500641 -0.460446<br/>
0.50835 -0.492593<br/>
-0.50835 -0.425924<br/>
0.514748 -0.491928<br/>
-0.514748 -0.432914<br/>
0.519522 -0.47324<br/>
-0.519522 -0.461528<br/>
0.522423 -0.435009<br/>
-0.522423 -0.513283<br/>
0.523281 -0.437947<br/>
-0.523281 -0.527471<br/>
0.522013 -0.46169<br/>
-0.522013 -0.524454<br/>
0.518626 -0.507305<br/>
-0.518626 -0.503164<br/>
0.513225 -0.513229<br/>
-0.513225 -0.525165<br/>
0.506006 -0.562888<br/>
-0.506006 -0.507031<br/>
0.497248 -0.573942<br/>
-0.497248 -0.531103<br/>
0.487306 -0.566351<br/>
-0.487306 -0.577419<br/>
0.476597 -0.602205<br/>
-0.476597 -0.583891<br/>
0.465579 -0.598083<br/>
-0.465579 -0.633938<br/>
0.454734 -0.636866<br/>
-0.454734 -0.64468<br/>
0.44455 -0.698841<br/>
-0.44455 -0.63583<br/>
0.435492 -0.722352<br/>
-0.435492 -0.669045<br/>
0.427989 -0.72752<br/>
-0.427989 -0.724202<br/>
0.422409 -0.712815<br/>
-0.422409 -0.802832<br/>
0.419041 -0.739154<br/>
-0.419041 -0.844019<br/>
0.418087 -0.785905<br/>
-0.418087 -0.868393<br/>
0.41964 -0.85398<br/>
-0.41964 -0.875044<br/>
0.423689 -0.945135<br/>
-0.423689 -0.862213<br/>
0.430107 -0.945135<br/>
-0.430107 -0.890206<br/>
0.438664 -0.855494<br/>
-0.438664 -0.940162<br/>
0.449026 -0.806105<br/>
-0.449026 -0.953467<br/>
0.460772 -0.775404<br/>
-0.460772 -0.951684<br/>
0.473412 -0.763163<br/>
-0.473412 -0.935039<br/>
0.486404 -0.769896<br/>
-0.486404 -0.903022<br/>
0.499177 -0.796875<br/>
-0.499177 -0.854358<br/>
0.511159 -0.846188<br/>
-0.511159 -0.78696<br/>
0.521801 -0.858049<br/>
-0.521801 -0.760614<br/>
0.530599 -0.853344<br/>
-0.530599 -0.754435<br/>
0.537122 -0.831631<br/>
-0.537122 -0.768863<br/>
0.541028 -0.791639<br/>
-0.541028 -0.805171<br/>
0.542087 -0.79443<br/>
-0.542087 -0.802295<br/>
0.540188 -0.820047<br/>
-0.540188 -0.780193<br/>
0.535349 -0.806598<br/>
-0.535349 -0.800758<br/>
0.52772 -0.773482<br/>
-0.52772 -0.844589<br/>
0.517579 -0.782137<br/>
-0.517579 -0.850249<br/>
0.50532 -0.812391<br/>
-0.50532 -0.837911<br/>
0.491441 -0.86566<br/>
-0.491441 -0.806157<br/>
0.476523 -0.880432<br/>
-0.476523 -0.816501<br/>
0.461206 -0.877158<br/>
-0.461206 -0.84849<br/>
0.446162 -0.854602<br/>
-0.446162 -0.903361<br/>
0.432064 -0.874493<br/>
-0.432064 -0.919386<br/>
<br/>

## Diagrama de classes

Para essa fase do trabalho foi utilizado o Doxygen e para verificar o diagrama
de classes, basta entrar na pasta e abrir o arquivo index.html em seu navegador.
 ![Alt text](html/classSpring__coll__graph.png?raw=true "")
 
## Gráfico 
- Springmass ![Alt text](grafico_springmass.png?raw=true "Optional Title")


# Parte 3 - Graphics

## Introdução

A Graphics proporciona uma interface gráfica para o Bouncing Ball e Springmass,
que já foram descritos acima.

## Dependências do projeto

Esse projeto necessita do uso do OpenGL e da Glut, que são responsáveis por fornecer
interfaces em 2D e 3D, para maior interação das funcionalidades com a parte visual.

- Para instalar a OpenGL: sudo apt-get install freeglut3 freeglut3-dev, para Ubuntu.


## Como compilar

 - ```make```
 - ```./test-ball-graphics```
 - ```./test-springmass-graphics```

## Descrição Graphics

O repositório contém um arquivo springmass.cpp, além disso possui a springmass.h,
que é seu header e o test-springmass.cpp, que possui as funções para visualizar o
springmass.cpp. Se trata de um sistema com um oscilador harmônico amortecido, onde
são feitos cálculos de força, velocidade, energia total, entre outros. Também
relacionado está o Bouncing Ball descrito acima. A Graphics, cujo descrição está
em questão, possui a graphics.h, graphics.cpp, test-ball-graphics e
test-springmass-graphics.cpp.


## Diagrama de classes

Para essa fase do trabalho foi utilizado o Doxygen e para verificar o diagrama
de classes, basta entrar na pasta e abrir o arquivo index.html em seu navegador.
 ![Alt text](html/classDrawable__inherit__graph.png?raw=true "")
 
## Screenshots 
- Bouncing Ball ![Alt text](Capture_boucing.png?raw=true "Optional Title")
- Springmass (não rodou!)
